resource "helm_release" "secure_keycloak" {
  depends_on = [
    module.postgres_module
  ]
  name       = "keycloak"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "keycloak"

  set {
    name  = "httpRelativePath"
    value = "keycloak/"
  }
  set {
    name  = "auth.adminUser"
    value = "admin"
  }
  set {
    name  = "auth.adminPassword"
    value = "secretpassword"
  }
  set {
    name  = "production"
    value = "true"
  }
  set {
    name  = "proxy"
    value = "edge"
  }
  set {
    name  = "postgresql.enabled"
    value = "false"
  }
  set {
    name  = "externalDatabase.database"
    value = "keycloak"
  }
  set {
    name  = "externalDatabase.host"
    value = "postgres-postgresql"
  }
  set {
    name  = "externalDatabase.user"
    value = var.db-username
  }
  set {
    name  = "externalDatabase.password"
    value = var.db-password
  }

  # set {
  #   name  = "extraStartupArgs"
  #   value = "--hostname=keycloak.default.svc.cluster.local --hostname-admin=keycloak.local"
  # }
   set {
    name  = "extraStartupArgs"
    value = "--hostname-url=http://keycloak.local/keycloak"
  }
}

resource "kubernetes_ingress_v1" "keycloak_ingress" {
  metadata {
    name = "keycloak-ingress"
    annotations = {
      "nginx.ingress.kubernetes.io/configuration-snippet" = <<EOF
        proxy_set_header    Host               $host;
        proxy_set_header    X-Real-IP          $remote_addr;
        proxy_set_header    X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Host   $host;
        proxy_set_header    X-Forwarded-Server $host;
        proxy_set_header    X-Forwarded-Port   $server_port;
        EOF
    }
  }
  spec {
    ingress_class_name = "default"
    rule {
      host = "keycloak.local"
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "keycloak"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}
